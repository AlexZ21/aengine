#include "glfwevent.h"

AENGINE::GLFWEvent::GLFWEvent()
{

}

AENGINE::GLFWEvent::~GLFWEvent()
{
}

void AENGINE::GLFWEvent::setGLFWWindow(GLFWwindow *w)
{
    window = w;
}

bool AENGINE::GLFWEvent::keyPress(int key)
{
    return keyStateArray[key];
}

bool AENGINE::GLFWEvent::keyHit(int key)
{
    if(keyStateArray[key])
    {
        if(!keyHitStateArray[key])
        {
            keyHitStateArray[key] = true;
            return true;
        }else{
            return false;
        }
    }else{
        if(keyHitStateArray[key])
            keyHitStateArray[key] = false;
        return false;
    }
}

bool AENGINE::GLFWEvent::mousePress(int but)
{
    return buttonStateArray[but];
}

bool AENGINE::GLFWEvent::mouseHit(int but)
{
    if(buttonStateArray[but])
    {
        if(!buttonHitStateArray[but])
        {
            buttonHitStateArray[but] = true;
            return true;
        }else{
            return false;
        }
    }else{
        if(buttonHitStateArray[but])
            buttonHitStateArray[but] = false;
        return false;
    }
}

void AENGINE::GLFWEvent::setCursorPostion(int x, int y)
{
    glfwSetCursorPos(window, x, y);
    mouseOldX = x;
    mouseOldY = y;
    mouseX = x;
    mouseY = y;
}

double AENGINE::GLFWEvent::getCursorPosX()
{
    return mouseX;
}

double AENGINE::GLFWEvent::getCursorPosY()
{
    return mouseY;
}

double AENGINE::GLFWEvent::getCursorSpeedX()
{
    return mouseX-mouseOldX;
}

double AENGINE::GLFWEvent::getCursorSpeedY()
{
    return mouseY-mouseOldY;
}


void AENGINE::GLFWEvent::updateKeys(int k, bool state)
{
    keyStateArray[k] = state;
}

void AENGINE::GLFWEvent::updateButtons(int b, bool state)
{
    buttonStateArray[b] = state;
}

void AENGINE::GLFWEvent::updateCursorPosition()
{
    mouseOldX = mouseX;
    mouseOldY = mouseY;
    glfwGetCursorPos(window, &mouseX, &mouseY);
}
