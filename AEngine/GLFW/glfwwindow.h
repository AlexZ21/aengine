#ifndef GLFWWINDOW_H
#define GLFWWINDOW_H

#ifdef __WIN32__
    #include <GL/glfw3.h>
#elif __linux__
    #include <GLFW/glfw3.h>
#endif

#include "../awindow.h"
#include "../log.h"
#include "glfwevent.h"


namespace AENGINE {

class GLFWWindow: public AWindow
{
    int width, height;
    bool fullscreen;
    GLFWwindow* window;
    const char *title;

    GLFWEvent event;
public:
    GLFWWindow(int w, int h, bool fullscreen);
    ~GLFWWindow();

    void setWindowTitle(const char *title);
    bool shouldExit();
    void setShouldExit(bool e);

    bool isFullscreen();
    void setFullscreen(bool f);

    void updateSizeWindow();
    void update();

    void initGL();

    float getWidth();
    float getHeight();

    AEvent *getEventHandler();

    static void keyCallback(GLFWwindow* w, int key, int scancode, int action, int mods);
    static void mouseCallback(GLFWwindow* w, int button, int action, int mods);
};

}

#endif // GLFWWINDOW_H
