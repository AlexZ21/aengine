#ifndef GLFWEVENT_H
#define GLFWEVENT_H

#ifdef __WIN32__
    #include <GL/glfw3.h>
#elif __linux__
    #include <GLFW/glfw3.h>
#endif

#include "../aevent.h"
#include "../log.h"
#include <map>

namespace AENGINE {

class GLFWEvent: public AEvent
{
    std::map<int, bool> keyStateArray;
    std::map<int, bool> buttonStateArray;

    std::map<int, bool> keyHitStateArray;
    std::map<int, bool> buttonHitStateArray;

    GLFWwindow* window;

    //Mouse
    double mouseX, mouseY;
    double mouseOldX, mouseOldY;
public:
    GLFWEvent();
    ~GLFWEvent();

    void setGLFWWindow(GLFWwindow* w);

    //Keyboard
    bool keyPress(int key);
    bool keyHit(int key);

    //Mouse
    bool mousePress(int but);
    bool mouseHit(int but);

    void setCursorPostion(int x, int y);
    double getCursorPosX();
    double getCursorPosY();
    double getCursorSpeedX();
    double getCursorSpeedY();

    //Update
    void updateKeys(int k, bool state);
    void updateButtons(int b, bool state);
    void updateCursorPosition();

};

}

#endif // GLFWEVENT_H
