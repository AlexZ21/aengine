#include "glfwwindow.h"
#include <math.h>

//AENGINE::GLFWEvent AENGINE::GLFWWindow::event;

AENGINE::GLFWWindow::GLFWWindow(int w, int h, bool fullscreen)
{
    width = w;
    height = h;
    this->fullscreen = fullscreen;

    int count;
    const GLFWvidmode *modes = glfwGetVideoModes(glfwGetPrimaryMonitor(), &count);

    for(int i = 0; i < count; i++)
    {
        Log::print() << i << ". " << modes[i].width << " x " << modes[i].height << ", " << modes[i].refreshRate
                     << "MHz, red:" <<  modes[i].redBits << " green:" << modes[i].greenBits
                     << " blue:" << modes[i].blueBits;

    }


    if(fullscreen)
        window = glfwCreateWindow(w, h, "new window", glfwGetPrimaryMonitor(), NULL);
    else
        window = glfwCreateWindow(w, h, "new window", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return;
    }


    event.setGLFWWindow(window);
    glfwSetWindowUserPointer(window, &event);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, GLFWWindow::keyCallback);
    glfwSetMouseButtonCallback(window, GLFWWindow::mouseCallback);

    initGL();
}

AENGINE::GLFWWindow::~GLFWWindow()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void AENGINE::GLFWWindow::setWindowTitle(const char *title)
{
    this->title = title;
    glfwSetWindowTitle (window, title);
}

bool AENGINE::GLFWWindow::shouldExit()
{
    return glfwWindowShouldClose(window);
}

void AENGINE::GLFWWindow::setShouldExit(bool e)
{
    if(e)
        glfwSetWindowShouldClose(window, GL_TRUE);
    else
        glfwSetWindowShouldClose(window, GL_FALSE);

}

bool AENGINE::GLFWWindow::isFullscreen()
{
    return fullscreen;
}

void AENGINE::GLFWWindow::setFullscreen(bool f)
{
    if(fullscreen != f)
    {
        fullscreen = f;

        if(window)
        {
            glfwDestroyWindow(window);

            if(fullscreen)
                window = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(), NULL);
            else
                window = glfwCreateWindow(width, height, title, NULL, NULL);

            if (!window)
            {
                glfwTerminate();
                return;
            }

            event.setGLFWWindow(window);
            glfwSetWindowUserPointer(window, &event);
            glfwMakeContextCurrent(window);
            glfwSwapInterval(1);
            glfwSetKeyCallback(window, GLFWWindow::keyCallback);
            glfwSetMouseButtonCallback(window, GLFWWindow::mouseCallback);

            initGL();

        }

    }
}

void AENGINE::GLFWWindow::updateSizeWindow()
{  
    glfwGetFramebufferSize(window, &width, &height);
}

void AENGINE::GLFWWindow::update()
{
    event.updateCursorPosition();
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void AENGINE::GLFWWindow::initGL()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
}

float AENGINE::GLFWWindow::getWidth()
{
   return width;
}

float AENGINE::GLFWWindow::getHeight()
{
   return height;
}

AENGINE::AEvent *AENGINE::GLFWWindow::getEventHandler()
{
    Log::print() << "get event handler";
    return &event;
}

void AENGINE::GLFWWindow::keyCallback(GLFWwindow *w, int key, int scancode, int action, int mods)
{
    GLFWEvent *event = (GLFWEvent*)glfwGetWindowUserPointer(w);
    if(action == GLFW_PRESS)
        event->updateKeys(key, true);
    else if(action == GLFW_RELEASE)
        event->updateKeys(key, false);

}

void AENGINE::GLFWWindow::mouseCallback(GLFWwindow *w, int button, int action, int mods)
{
    GLFWEvent *event = (GLFWEvent*)glfwGetWindowUserPointer(w);
    if(action == GLFW_PRESS)
        event->updateButtons(button, true);
    else if(action == GLFW_RELEASE)
        event->updateButtons(button, false);
}
