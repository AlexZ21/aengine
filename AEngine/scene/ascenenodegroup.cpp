#include "ascenenodegroup.h"

AENGINE::ASceneNodeGroup::ASceneNodeGroup()
{
    visible = true;
    Log::print() << "Create node group " << this;
}

AENGINE::ASceneNodeGroup::ASceneNodeGroup(AScene *scene)
{
    visible = true;
    Log::print() << "Create node group " << this;
    scene->addSceneNodeGroup(this);
}

AENGINE::ASceneNodeGroup::~ASceneNodeGroup()
{
    removeAllSceneNode();
    Log::print() << "--Delete node group " << this;
}



//Add scene node
void AENGINE::ASceneNodeGroup::addSceneNode(AENGINE::AAbstractSceneNode *sceneNode)
{
    if(sceneNode)
    {
        Log::print() << "Add scene node " << sceneNode;
        sceneNodeList.push_back(sceneNode);
    }else{
        Log::print()<<"Error add scene node";
    }
}

void AENGINE::ASceneNodeGroup::removeSceneNode(AENGINE::AAbstractSceneNode *sceneNode)
{
    if(sceneNode)
    {
        delete sceneNode;
        sceneNodeList.remove(sceneNode);
        sceneNode = NULL;
    }else{
        Log::print()<<"Error remove scene node";
    }
}

void AENGINE::ASceneNodeGroup::removeAllSceneNode()
{
    if(!sceneNodeList.empty())
    {
        for(std::list<AAbstractSceneNode *>::iterator it = sceneNodeList.begin(); it != sceneNodeList.end(); ++it)
            delete *it;
        sceneNodeList.clear();
        Log::print()<<"--Removed all scene";
    }else{
        Log::print()<<"Scene list is empty";
    }
}



//Update scene node group
void AENGINE::ASceneNodeGroup::setVisible(bool visible)
{
    this->visible = visible;
}

bool AENGINE::ASceneNodeGroup::isVisible()
{
    return visible;
}

void AENGINE::ASceneNodeGroup::update()
{
    if(visible)
    {
                for(std::list<AAbstractSceneNode *>::iterator it = sceneNodeList.begin(); it != sceneNodeList.end(); ++it)
                    (*it)->draw();
    }

}
