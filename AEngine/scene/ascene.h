#ifndef ASCENE_H
#define ASCENE_H

#include "ascenemanager.h"
#include "ascenenodegroup.h"
#include "../log.h"
#include <list>

namespace AENGINE {

class ASceneManager;
class ASceneNodeGroup;

class AScene
{
   bool visible;

   std::list<ASceneNodeGroup *> sceneNodeGroupList;
   ASceneNodeGroup *rootSceneNodeGroup;

public:
    AScene();
    AScene(ASceneManager *sceneManager);
    ~AScene();

    //Node group
    void addSceneNodeGroup(ASceneNodeGroup *sceneNodeGroup);
    void removeSceneNodeGroup(ASceneNodeGroup *sceneNodeGroup);
    ASceneNodeGroup *getRootSceneNodeGroup();

    //scene state
    void setVisible(bool visible);
    bool isVisible();

    //scene update
    void update();
};

}

#endif // ASCENE_H
