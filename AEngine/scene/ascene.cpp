#include "ascene.h"

AENGINE::AScene::AScene()
{
    visible = true;

    rootSceneNodeGroup = new ASceneNodeGroup();
    sceneNodeGroupList.push_back(rootSceneNodeGroup);

    Log::print() << "Create scene " << this;
}

AENGINE::AScene::AScene(ASceneManager *sceneManager)
{
    visible = true;
    sceneManager->addScene(this);
    Log::print() << "Create scene " << this;
}

AENGINE::AScene::~AScene()
{
    if(!sceneNodeGroupList.empty())
    {
        for(std::list<ASceneNodeGroup*>::iterator it = sceneNodeGroupList.begin(); it != sceneNodeGroupList.end(); ++it)
            delete *it;
        sceneNodeGroupList.clear();
        Log::print()<<"--Removed all node group";
    }else{
        Log::print()<<"Node group list is empty";
    }

    Log::print() << "--Delete scene " << this;
}




//Node group
void AENGINE::AScene::addSceneNodeGroup(AENGINE::ASceneNodeGroup *sceneNodeGroup)
{
    if(sceneNodeGroup)
    {
        Log::print()<<"Add scene node group " << sceneNodeGroup;
        sceneNodeGroupList.push_back(sceneNodeGroup);
    }else{
        Log::print()<<"Erorr add scene node group";
    }
}

void AENGINE::AScene::removeSceneNodeGroup(AENGINE::ASceneNodeGroup *sceneNodeGroup)
{
    if(sceneNodeGroup)
    {
        Log::print()<<"--Remove scene node group" << sceneNodeGroup;
        delete sceneNodeGroup;
        sceneNodeGroupList.remove(sceneNodeGroup);
    }else{
        Log::print()<<"Erorr remove scene node group";
    }
}

AENGINE::ASceneNodeGroup *AENGINE::AScene::getRootSceneNodeGroup()
{
    if(rootSceneNodeGroup)
        return rootSceneNodeGroup;
    else
        return NULL;
}




//Scene state
void AENGINE::AScene::setVisible(bool visible)
{
    this->visible = visible;
}

bool AENGINE::AScene::isVisible()
{
    return visible;
}




//Scene update
void AENGINE::AScene::update()
{
    if(!sceneNodeGroupList.empty())
        for(std::list<ASceneNodeGroup*>::iterator it = sceneNodeGroupList.begin(); it != sceneNodeGroupList.end(); ++it)
        {
            (*it)->update();

        }
}

