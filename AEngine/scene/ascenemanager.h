#ifndef ASCENEMANAGER_H
#define ASCENEMANAGER_H

#include <list>
#include "../log.h"
#include "ascene.h"
#include "scene_node/ascenecamera.h"

namespace AENGINE {

class AScene;
//class ASceneCamera;
class AAbstractSceneNode;

class ASceneManager
{
    std::list<AScene *> sceneList;

    //Camera
    ASceneCamera *sceneCamera;
public:
    ASceneManager();
    ~ASceneManager();

    AScene *createScene();
    void addScene(AScene *scene);
    void removeScene(AScene *scene);
    void removeAllScene();

    //Create camera
    ASceneCamera *createSceneCamera();

    void update();
};

}

#endif // ASCENEMANAGER_H
