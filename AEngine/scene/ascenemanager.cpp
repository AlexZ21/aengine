#include "ascenemanager.h"


AENGINE::ASceneManager::ASceneManager()
{
    sceneCamera = NULL;
}

AENGINE::ASceneManager::~ASceneManager()
{
    removeAllScene();
}

AENGINE::AScene *AENGINE::ASceneManager::createScene()
{
    AScene *newScene = new AScene();
    if(newScene)
    {
        sceneList.push_back(newScene);
        return newScene;
    }else{
        Log::print()<<"Error create new scene";
        return 0;
    }
}

void AENGINE::ASceneManager::addScene(AENGINE::AScene *scene)
{
    if(scene)
    {
        Log::print() << "Add scene " << scene;
        sceneList.push_back(scene);
    }else{
        Log::print()<<"Error add scene";
    }
}

void AENGINE::ASceneManager::removeScene(AENGINE::AScene *scene)
{
    if(scene)
    {
        delete scene;
        sceneList.remove(scene);
        scene = NULL;
    }else{
        Log::print()<<"Error remove scene";
    }
}

void AENGINE::ASceneManager::removeAllScene()
{
    if(!sceneList.empty())
    {
        for(std::list<AScene*>::iterator it = sceneList.begin(); it != sceneList.end(); ++it)
            delete *it;
        sceneList.clear();
        Log::print()<<"--Removed all scene";
    }else{
        Log::print()<<"Scene list is empty";
    }
}

AENGINE::ASceneCamera *AENGINE::ASceneManager::createSceneCamera()
{
    if(sceneCamera == NULL) sceneCamera = new ASceneCamera();
    return sceneCamera;
}

void AENGINE::ASceneManager::update()
{
    if(sceneCamera != NULL) sceneCamera->draw();

    if(!sceneList.empty())
    {
        for(std::list<AScene*>::iterator it = sceneList.begin(); it != sceneList.end(); ++it)
        {
            (*it)->update();
        }
    }
}
