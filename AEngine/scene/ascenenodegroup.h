#ifndef ASCENENODEGROUP_H
#define ASCENENODEGROUP_H

#include <list>
#include "../log.h"
#include "ascene.h"
#include "scene_node/aabstractscenenode.h"

namespace AENGINE {

class AScene;
class AAbstractSceneNode;

class ASceneNodeGroup
{
    bool visible;
    AScene *scene;
    std::list<AAbstractSceneNode *> sceneNodeList;
public:
    ASceneNodeGroup();
    ASceneNodeGroup(AScene *scene);
    ~ASceneNodeGroup();


    void addSceneNode(AAbstractSceneNode *sceneNode);
    void removeSceneNode(AAbstractSceneNode *sceneNode);
    void removeAllSceneNode();


    void setVisible(bool visible);
    bool isVisible();

    void update();
};

}

#endif // ASCENENODEGROUP_H
