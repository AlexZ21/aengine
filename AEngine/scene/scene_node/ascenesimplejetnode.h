#ifndef ASCENESIMPLEJETNODE_H
#define ASCENESIMPLEJETNODE_H

#ifdef __WIN32__
    #include <GL/glfw3.h>
#elif __linux__
    #include <GLFW/glfw3.h>
#endif

#include "aabstractscenenode.h"

namespace AENGINE {

class ASceneSimpleJetNode : public AAbstractSceneNode
{
public:
    ASceneSimpleJetNode();
    ASceneSimpleJetNode(ASceneNodeGroup *nodeGroup);

    void draw();
};
}

#endif // ASCENESIMPLEJETNODE_H
