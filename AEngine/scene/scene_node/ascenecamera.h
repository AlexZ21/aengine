#ifndef ASCENECAMERA_H
#define ASCENECAMERA_H

#ifdef __WIN32__
#include <GL/glfw3.h>
#elif __linux__
#include <GLFW/glfw3.h>
#endif

#include "aabstractscenenode.h"


namespace AENGINE {

class ASceneManager;
class AAbstractSceneNode;

class ASceneCamera : public AAbstractSceneNode
{
    vector3 rotation;
    vector3 position;
public:
    ASceneCamera();


    //Set position
    void setPosition(const vector3 &pos);
    void setPositionX(float x);
    void setPositionY(float y);
    void setPositionZ(float z);

    //Set rotate
    void setRotation(const vector3 &rot);
    void setRotationX(float x);
    void setRotationY(float y);
    void setRotationZ(float z);

    //Update camera

    void update();
    void draw();
};
}

#endif // ASCENECAMERA_H
