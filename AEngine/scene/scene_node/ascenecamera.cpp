#include "ascenecamera.h"


AENGINE::ASceneCamera::ASceneCamera()
{
    position.setZero();
    rotation.setZero();
}

void AENGINE::ASceneCamera::setPosition(const AENGINE::vector3 &pos)
{
    position = pos;
}

void AENGINE::ASceneCamera::setPositionX(float x)
{
    position.setX(x);
}

void AENGINE::ASceneCamera::setPositionY(float y)
{
    position.setY(y);
}

void AENGINE::ASceneCamera::setPositionZ(float z)
{
    position.setZ(z);
}

void AENGINE::ASceneCamera::setRotation(const AENGINE::vector3 &rot)
{
    rotation = rot;
}

void AENGINE::ASceneCamera::setRotationX(float x)
{
    rotation.setX(x);
}

void AENGINE::ASceneCamera::setRotationY(float y)
{
    rotation.setY(y);
}

void AENGINE::ASceneCamera::setRotationZ(float z)
{
    rotation.setZ(z);
}

void AENGINE::ASceneCamera::update()
{

}

void AENGINE::ASceneCamera::draw()
{
    glLoadIdentity();

    glRotated(rotation.getZ(), 0.0, 0.0, 1.0);
    glRotated(rotation.getY(), 0.0, 1.0, 0.0);
    glRotated(rotation.getX(), 1.0, 0.0, 0.0);

    glTranslated(-position.getX(), -position.getY(), -position.getZ());
}
