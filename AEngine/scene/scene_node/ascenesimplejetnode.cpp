#include "ascenesimplejetnode.h"


AENGINE::ASceneSimpleJetNode::ASceneSimpleJetNode(): AAbstractSceneNode()
{

}

AENGINE::ASceneSimpleJetNode::ASceneSimpleJetNode(AENGINE::ASceneNodeGroup *nodeGroup): AAbstractSceneNode(nodeGroup)
{

}

void AENGINE::ASceneSimpleJetNode::draw()
{
    glPushMatrix();


    glTranslated(position.getX(), position.getY(), position.getZ());
    glRotated(rotation.getZ(), 0.0, 0.0, 1.0);
    glRotated(rotation.getY(), 0.0, 1.0, 0.0);
    glRotated(rotation.getX(), 1.0, 0.0, 0.0);
    glScalef(scale.getX(), scale.getY(), scale.getZ());


    glColor3ub(255, 255, 255);
    glBegin(GL_TRIANGLES);
    glVertex3f(0.0f, 0.0f, 60.0f);
    glVertex3f(-15.0f, 0.0f, 30.0f);
    glVertex3f(15.0f,0.0f,30.0f);

    // Black
    glColor3ub(0,0,0);
    glVertex3f(15.0f,0.0f,30.0f);
    glVertex3f(0.0f, 15.0f, 30.0f);
    glVertex3f(0.0f, 0.0f, 60.0f);

    // Red
    glColor3ub(255,0,0);
    glVertex3f(0.0f, 0.0f, 60.0f);
    glVertex3f(0.0f, 15.0f, 30.0f);
    glVertex3f(-15.0f,0.0f,30.0f);


    // Body of the Plane ////////////////////////
    // Green
    glColor3ub(0,255,0);
    glVertex3f(-15.0f,0.0f,30.0f);
    glVertex3f(0.0f, 15.0f, 30.0f);
    glVertex3f(0.0f, 0.0f, -56.0f);

    glColor3ub(255,255,0);
    glVertex3f(0.0f, 0.0f, -56.0f);
    glVertex3f(0.0f, 15.0f, 30.0f);
    glVertex3f(15.0f,0.0f,30.0f);

    glColor3ub(0, 255, 255);
    glVertex3f(15.0f,0.0f,30.0f);
    glVertex3f(-15.0f, 0.0f, 30.0f);
    glVertex3f(0.0f, 0.0f, -56.0f);

    ///////////////////////////////////////////////
    // Left wing
    // Large triangle for bottom of wing
    glColor3ub(128,128,128);
    glVertex3f(0.0f,2.0f,27.0f);
    glVertex3f(-60.0f, 2.0f, -8.0f);
    glVertex3f(60.0f, 2.0f, -8.0f);

    glColor3ub(64,64,64);
    glVertex3f(60.0f, 2.0f, -8.0f);
    glVertex3f(0.0f, 7.0f, -8.0f);
    glVertex3f(0.0f,2.0f,27.0f);

    glColor3ub(192,192,192);
    glVertex3f(60.0f, 2.0f, -8.0f);
    glVertex3f(-60.0f, 2.0f, -8.0f);
    glVertex3f(0.0f,7.0f,-8.0f);

    // Other wing top section
    glColor3ub(64,64,64);
    glVertex3f(0.0f,2.0f,27.0f);
    glVertex3f(0.0f, 7.0f, -8.0f);
    glVertex3f(-60.0f, 2.0f, -8.0f);

    // Tail section///////////////////////////////
    // Bottom of back fin
    glColor3ub(255,128,255);
    glVertex3f(-30.0f, -0.50f, -57.0f);
    glVertex3f(30.0f, -0.50f, -57.0f);
    glVertex3f(0.0f,-0.50f,-40.0f);

    // top of left side
    glColor3ub(255,128,0);
    glVertex3f(0.0f,-0.5f,-40.0f);
    glVertex3f(30.0f, -0.5f, -57.0f);
    glVertex3f(0.0f, 4.0f, -57.0f);

    // top of right side
    glColor3ub(255,128,0);
    glVertex3f(0.0f, 4.0f, -57.0f);
    glVertex3f(-30.0f, -0.5f, -57.0f);
    glVertex3f(0.0f,-0.5f,-40.0f);

    // back of bottom of tail
    glColor3ub(255,255,255);
    glVertex3f(30.0f,-0.5f,-57.0f);
    glVertex3f(-30.0f, -0.5f, -57.0f);
    glVertex3f(0.0f, 4.0f, -57.0f);

    // Top of Tail section left
    glColor3ub(255,0,0);
    glVertex3f(0.0f,0.5f,-40.0f);
    glVertex3f(3.0f, 0.5f, -57.0f);
    glVertex3f(0.0f, 25.0f, -65.0f);

    glColor3ub(255,0,0);
    glVertex3f(0.0f, 25.0f, -65.0f);
    glVertex3f(-3.0f, 0.5f, -57.0f);
    glVertex3f(0.0f,0.5f,-40.0f);

    // Back of horizontal section
    glColor3ub(128,128,128);
    glVertex3f(3.0f,0.5f,-57.0f);
    glVertex3f(-3.0f, 0.5f, -57.0f);
    glVertex3f(0.0f, 25.0f, -65.0f);

    glEnd(); // Of Jet

    glPopMatrix();
}
