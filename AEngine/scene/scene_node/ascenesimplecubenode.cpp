#include "ascenesimplecubenode.h"

AENGINE::ASceneSimpleCubeNode::ASceneSimpleCubeNode(): AAbstractSceneNode()
{
}

AENGINE::ASceneSimpleCubeNode::ASceneSimpleCubeNode(ASceneNodeGroup *nodeGroup): AAbstractSceneNode(nodeGroup)
{
}

void AENGINE::ASceneSimpleCubeNode::draw()
{
    glPushMatrix();


    glTranslated(position.getX(), position.getY(), position.getZ());
    glRotated(rotation.getZ(), 0.0, 0.0, 1.0);
    glRotated(rotation.getY(), 0.0, 1.0, 0.0);
    glRotated(rotation.getX(), 1.0, 0.0, 0.0);
    glScalef(scale.getX(), scale.getY(), scale.getZ());


    glBegin(GL_QUADS);
    /* top of cube */
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    /* bottom of cube */
    glColor3f(1.0f, 0.5f, 0.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    /* front of cube */
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    /* back of cube */
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    /* right side of cube */
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    /* left side of cube */
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glEnd();
    glPopMatrix();
}
