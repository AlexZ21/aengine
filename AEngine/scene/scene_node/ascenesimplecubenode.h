#ifndef ASCENESIMPLECUBENODE_H
#define ASCENESIMPLECUBENODE_H

#ifdef __WIN32__
    #include <GL/glfw3.h>
#elif __linux__
    #include <GLFW/glfw3.h>
#endif

#include "aabstractscenenode.h"

namespace AENGINE {

class ASceneSimpleCubeNode : public AAbstractSceneNode
{
public:
    ASceneSimpleCubeNode();
    ASceneSimpleCubeNode(ASceneNodeGroup *nodeGroup);

    void draw();
};
}

#endif // ASCENESIMPLECUBENODE_H
