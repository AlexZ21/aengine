#ifndef AABSTRACTSCENENODE_H
#define AABSTRACTSCENENODE_H

#include "../../maths/vector3.h"
#include "../ascenenodegroup.h"

namespace AENGINE {

class ASceneNodeGroup;
class ASceneManager;

class AAbstractSceneNode
{
public:
    AAbstractSceneNode();
    AAbstractSceneNode(ASceneNodeGroup *nodeGroup);

    vector3 position;
    vector3 rotation;
    vector3 scale;

    //Set position
    virtual void setPosition(const vector3 &pos);
    virtual void setPositionX(float x);
    virtual void setPositionY(float y);
    virtual void setPositionZ(float z);

    //Set rotate
    virtual void setRotation(const vector3 &rot);
    virtual void setRotationX(float x);
    virtual void setRotationY(float y);
    virtual void setRotationZ(float z);

    //Set scale
    virtual void setScale(const vector3 &sca);
    virtual void setScaleX(float x);
    virtual void setScaleY(float y);
    virtual void setScaleZ(float z);


    virtual void update();
    virtual void draw() = 0;
};

}

#endif // AABSTRACTSCENENODE_H
