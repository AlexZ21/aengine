#include "aabstractscenenode.h"

AENGINE::AAbstractSceneNode::AAbstractSceneNode()
{
    position.setZero();
    rotation.setZero();
    scale.setOne();
}

AENGINE::AAbstractSceneNode::AAbstractSceneNode(ASceneNodeGroup *nodeGroup)
{
    position.setZero();
    rotation.setZero();
    scale.setOne();
    nodeGroup->addSceneNode(this);
}


void AENGINE::AAbstractSceneNode::setPosition(const AENGINE::vector3 &pos)
{
    position = pos;
}

void AENGINE::AAbstractSceneNode::setPositionX(float x)
{
    position.setX(x);
}

void AENGINE::AAbstractSceneNode::setPositionY(float y)
{
    position.setY(y);
}

void AENGINE::AAbstractSceneNode::setPositionZ(float z)
{
    position.setZ(z);
}

void AENGINE::AAbstractSceneNode::setRotation(const AENGINE::vector3 &rot)
{
    rotation = rot;
}

void AENGINE::AAbstractSceneNode::setRotationX(float x)
{
    rotation.setX(x);
}

void AENGINE::AAbstractSceneNode::setRotationY(float y)
{
    rotation.setY(y);
}

void AENGINE::AAbstractSceneNode::setRotationZ(float z)
{
    rotation.setZ(z);
}

void AENGINE::AAbstractSceneNode::setScale(const AENGINE::vector3 &sca)
{
    scale = sca;
}

void AENGINE::AAbstractSceneNode::setScaleX(float x)
{
    scale.setX(x);
}

void AENGINE::AAbstractSceneNode::setScaleY(float y)
{
    scale.setX(y);
}

void AENGINE::AAbstractSceneNode::setScaleZ(float z)
{
    scale.setX(z);
}


void AENGINE::AAbstractSceneNode::update()
{

}
