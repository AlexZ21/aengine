#include "log.h"

Log Log::newLog;

void Log::startLog(const char *name, bool toConsole)
{
    newLog.nameFile = name;
    newLog.enable = toConsole;


    if(newLog.enable)
    {
        std::cout<<"Start log";
    }

    if(newLog.nameFile != NULL)
        if(!newLog.fileStream.is_open())
        {
            newLog.fileStream.open(newLog.nameFile);
            newLog.fileStream<<newLog.nameFile<<" - start log";
            newLog.fileStream.close();
            newLog.fileStream.open(newLog.nameFile, std::ofstream::app);
        }
}

Log &Log::print()
{
    newLog.t = time(NULL);
    newLog.aTm = localtime(&newLog.t);

    std::cout.flush();
    if(newLog.enable)
        std::cout<<"\n"<<newLog.nameFile<<" "<<newLog.aTm->tm_hour<<":"<<newLog.aTm->tm_min<<":"<<newLog.aTm->tm_sec<<" - ";

    newLog.fileStream.flush();
    if(newLog.fileStream.is_open())
        newLog.fileStream<<"\n"<<newLog.aTm->tm_hour<<":"<<newLog.aTm->tm_min<<":"<<newLog.aTm->tm_sec<<" - ";

    return newLog;
}

void Log::closeLog()
{
    newLog.fileStream.close();
}

Log::Log()
{

}

Log::~Log()
{
}
