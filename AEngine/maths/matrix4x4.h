#ifndef MATRIX4X4_H
#define MATRIX4X4_H

#include "vector3.h"
#include "vector4.h"

namespace AENGINE {

class Matrix4x4
{
    float entries[16];
public:

    Matrix4x4();
    Matrix4x4(float e0, float e1, float e2, float e3,
              float e4, float e5, float e6, float e7,
              float e8, float e9, float e10, float e11,
              float e12, float e13, float e14, float e15);
    Matrix4x4(const float * f);
    Matrix4x4(const Matrix4x4 &mat);
    ~Matrix4x4() {}

    void setEntry(int position, float value);
    float getEntry(int position) const;
    Vector4 getRow(int position) const;
    Vector4 getColumn(int position) const;

    void setRow(int position, const Vector4& row);
    void setColumn(int position, const Vector4& row);

    void loadIdentity(void);
    void loadZero(void);

    Matrix4x4 operator+(const Matrix4x4 &mat) const;
    Matrix4x4 operator-(const Matrix4x4 &mat) const;
    Matrix4x4 operator*(const Matrix4x4 &mat) const;
    Matrix4x4 operator*(const float f) const;
    Matrix4x4 operator/(const float f) const;
    friend Matrix4x4 operator*(float scaleFactor, const Matrix4x4 &mat)
    {
        return mat*scaleFactor;
    }

    bool operator==(const Matrix4x4 &mat) const;
    bool operator!=(const Matrix4x4 &mat) const;

    void operator+=(const Matrix4x4 &mat);
    void operator-=(const Matrix4x4 &mat);
    void operator*=(const Matrix4x4 &mat);
    void operator*=(const float f);
    void operator/=(const float f);

    Matrix4x4 operator-(void) const;
    Matrix4x4 operator+(void) const;

    Vector4 operator*(const Vector4 vec) const;

    void rotateVector3D(Vector3 &vec) const;

    void inverseRotateVector3D(Vector3 &vec) const;

    Vector3 getRotatedVector3D(const Vector3 &vec) const;
    Vector3 getInverseRotatedVector3D(const Vector3 &vec) const;

    void translateVector3D(Vector3 &vec) const;

    void inverseTranslateVector3D(Vector3 &vec) const;

    Vector3 getTranslatedVector3D(const Vector3 &vec) const;
    Vector3 getInverseTranslatedVector3D(const Vector3 &vec) const;

    void invert(void);
    Matrix4x4 getInverse(void) const;
    void transpose(void);
    Matrix4x4 getTranspose(void) const;
    void invertTranspose(void);
    Matrix4x4 getInverseTranspose(void) const;

    void affineInvert(void);
    Matrix4x4 getAffineInverse(void) const;
    void affineInvertTranspose(void);
    Matrix4x4 getAffineInverseTranspose(void) const;

    void setTranslation(const Vector3 &translation);
    void setScale(const Vector3 &scaleFactor);
    void setUniformScale(const float scaleFactor);
    void setRotationAxis(const double angle, const Vector3 &axis);
    void setRotationX(const double angle);
    void setRotationY(const double angle);
    void setRotationZ(const double angle);
    void setRotationEuler(const double angleX, const double angleY, const double angleZ);
    void setPerspective(float left, float right, float bottom, float top, float n, float f);
    void setPerspective(float fovy, float aspect, float n, float f);
    void setOrtho(float left, float right, float bottom, float top, float n, float f);

    void setTranslationPart(const Vector3 &translation);
    void setRotationPartEuler(const float angleX, const float angleY, const float angleZ);
    void setRotationPartEuler(const Vector3 &rotations);

    operator float* () const {return (float*) this;}
    operator const float* () const {return (const float*) this;}

};

}

#endif // MATRIX4X4_H
