#ifndef VECTOR3_H
#define VECTOR3_H

#include <math.h>
#include "../log.h"

namespace AENGINE{

class vector3
{
    float x, y, z;
public:
    vector3();
    vector3(float x, float y, float z);
    vector3(const vector3 &vec);

    //Set one
    void setOne();

    //Set zero
    void setZero();

    //Set value for x, y, z
    void setX(float x);
    void setY(float y);
    void setZ(float z);

    //Get value
    float getX();
    float getY();
    float getZ();

    //Set scale a vector: x = x * value...
    void setScale(float value);

    //Get lenght of vector: sqrt(x*x + y*y + z*z)
    float getLenght();

    //Set a unit vector
    void normalize();

    //Calculate the cross product of two vectors
    void crossProduct(const vector3& vectorA, const vector3& vectorB);

};
}

#endif // VECTOR3_H
