#include "vector3.h"


AENGINE::vector3::vector3()
{
    x = 0;
    y = 0;
    z = 0;
}

AENGINE::vector3::vector3(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

AENGINE::vector3::vector3(const AENGINE::vector3 &vec)
{
    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
}

void AENGINE::vector3::setOne()
{
    x = 1;
    y = 1;
    z = 1;
}

void AENGINE::vector3::setZero()
{
    x = 0;
    y = 0;
    z = 0;
}

void AENGINE::vector3::setX(float x)
{
    this->x = x;
}

void AENGINE::vector3::setY(float y)
{
    this->y = y;
}

void AENGINE::vector3::setZ(float z)
{
    this->z = z;
}

float AENGINE::vector3::getX()
{
    return this->x;
}

float AENGINE::vector3::getY()
{
    return this->y;
}

float AENGINE::vector3::getZ()
{
    return this->z;
}

void AENGINE::vector3::setScale(float value)
{
    this->x *= value;
    this->y *= value;
    this->z *= value;
}

float AENGINE::vector3::getLenght()
{
    return (float)sqrt((this->x*this->x)+(this->y*this->y)+(this->z*this->z));
}

void AENGINE::vector3::normalize()
{
    setScale((float)1/getLenght());
}

void AENGINE::vector3::crossProduct(const AENGINE::vector3 &vectorA, const AENGINE::vector3 &vectorB)
{
    this->x = vectorA.y*vectorB.z - vectorB.y*vectorA.z;
    this->y = -vectorA.x*vectorB.z + vectorB.x*vectorA.z;
    this->z = vectorA.x*vectorB.y - vectorB.x*vectorA.y;
}
