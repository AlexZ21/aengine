#include "matrix4x4.h"
#include <memory.h>
#include <string.h>

AENGINE::Matrix4x4::Matrix4x4()
{

}

AENGINE::Matrix4x4::Matrix4x4(float e0, float e1, float e2, float e3,
                              float e4, float e5, float e6, float e7,
                              float e8, float e9, float e10, float e11,
                              float e12, float e13, float e14, float e15)
{
    entries[0]=e0;
    entries[1]=e1;
    entries[2]=e2;
    entries[3]=e3;
    entries[4]=e4;
    entries[5]=e5;
    entries[6]=e6;
    entries[7]=e7;
    entries[8]=e8;
    entries[9]=e9;
    entries[10]=e10;
    entries[11]=e11;
    entries[12]=e12;
    entries[13]=e13;
    entries[14]=e14;
    entries[15]=e15;
}

AENGINE::Matrix4x4::Matrix4x4(const float *f)
{
    memcpy(entries, f, 16*sizeof(float));
}

AENGINE::Matrix4x4::Matrix4x4(const AENGINE::Matrix4x4 &mat)
{
    memcpy(entries, mat.entries, 16*sizeof(float));
}

void AENGINE::Matrix4x4::setEntry(int position, float value)
{
    if(position>=0 && position<=15)
        entries[position]=value;
}

float AENGINE::Matrix4x4::getEntry(int position) const
{
    if(position>=0 && position<=15)
        return entries[position];
    else
        return 0.0f;
}

AENGINE::Vector4 AENGINE::Matrix4x4::getRow(int position) const
{
    if(position==0)
        return Vector4(entries[0], entries[4], entries[8], entries[12]);

    if(position==1)
        return Vector4(entries[1], entries[5], entries[9], entries[13]);

    if(position==2)
        return Vector4(entries[2], entries[6], entries[10], entries[14]);

    if(position==3)
        return Vector4(entries[3], entries[7], entries[11], entries[15]);

    return Vector4(0.0f, 0.0f, 0.0f, 0.0f);
}

AENGINE::Vector4 AENGINE::Matrix4x4::getColumn(int position) const
{
    if(position==0)
        return Vector4(entries[0], entries[1], entries[2], entries[3]);

    if(position==1)
        return Vector4(entries[4], entries[5], entries[6], entries[7]);

    if(position==2)
        return Vector4(entries[8], entries[9], entries[10], entries[11]);

    if(position==3)
        return Vector4(entries[12], entries[13], entries[14], entries[15]);

    return Vector4(0.0f, 0.0f, 0.0f, 0.0f);
}

void AENGINE::Matrix4x4::setRow(int position, const AENGINE::Vector4 &row)
{
    if(position==0) {
        entries[0] = row.getX();
        entries[4] = row.getY();
        entries[8] = row.getZ();
        entries[12] = row.getW();
    }
    else if(position==1) {
        entries[1] = row.getX();
        entries[5] = row.getY();
        entries[9] = row.getZ();
        entries[13] = row.getW();
    }
    else if(position==2) {
        entries[2] = row.getX();
        entries[6] = row.getY();
        entries[10] = row.getZ();
        entries[14] = row.getW();
    }
    else {
        entries[3] = row.getX();
        entries[7] = row.getY();
        entries[11] = row.getZ();
        entries[15] = row.getW();
    }
}

void AENGINE::Matrix4x4::setColumn(int position, const AENGINE::Vector4 &row)
{
    if(position==0) {
        entries[0] = row.getX();
        entries[1] = row.getY();
        entries[2] = row.getZ();
        entries[3] = row.getW();
    }
    else if(position==1) {
        entries[4] = row.getX();
        entries[5] = row.getY();
        entries[6] = row.getZ();
        entries[7] = row.getW();
    }
    else if(position==2) {
        entries[8] = row.getX();
        entries[9] = row.getY();
        entries[10] = row.getZ();
        entries[11] = row.getW();
    }
    else {
        entries[12] = row.getX();
        entries[13] = row.getY();
        entries[14] = row.getZ();
        entries[15] = row.getW();
    }
}

void AENGINE::Matrix4x4::loadIdentity()
{
    memset(entries, 0, 16*sizeof(float));
    entries[0]=1.0f;
    entries[5]=1.0f;
    entries[10]=1.0f;
    entries[15]=1.0f;
}

void AENGINE::Matrix4x4::loadZero()
{
    memset(entries, 0, 16*sizeof(float));
}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator+(const AENGINE::Matrix4x4 &mat) const
{
    return Matrix4x4(    entries[0]+mat.entries[0],
            entries[1]+mat.entries[1],
            entries[2]+mat.entries[2],
            entries[3]+mat.entries[3],
            entries[4]+mat.entries[4],
            entries[5]+mat.entries[5],
            entries[6]+mat.entries[6],
            entries[7]+mat.entries[7],
            entries[8]+mat.entries[8],
            entries[9]+mat.entries[9],
            entries[10]+mat.entries[10],
            entries[11]+mat.entries[11],
            entries[12]+mat.entries[12],
            entries[13]+mat.entries[13],
            entries[14]+mat.entries[14],
            entries[15]+mat.entries[15]);
}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator-(const AENGINE::Matrix4x4 &mat) const
{
    return Matrix4x4(    entries[0]-mat.entries[0],
            entries[1]-mat.entries[1],
            entries[2]-mat.entries[2],
            entries[3]-mat.entries[3],
            entries[4]-mat.entries[4],
            entries[5]-mat.entries[5],
            entries[6]-mat.entries[6],
            entries[7]-mat.entries[7],
            entries[8]-mat.entries[8],
            entries[9]-mat.entries[9],
            entries[10]-mat.entries[10],
            entries[11]-mat.entries[11],
            entries[12]-mat.entries[12],
            entries[13]-mat.entries[13],
            entries[14]-mat.entries[14],
            entries[15]-mat.entries[15]);
}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator*(const AENGINE::Matrix4x4 &mat) const
{
    //Optimise for matrices in which bottom row is (0, 0, 0, 1) in both matrices
    if(    entries[3]==0.0f && entries[7]==0.0f && entries[11]==0.0f && entries[15]==1.0f    &&
           mat.entries[3]==0.0f && mat.entries[7]==0.0f &&
           mat.entries[11]==0.0f && mat.entries[15]==1.0f)
    {
        return Matrix4x4(    entries[0]*mat.entries[0]+entries[4]*mat.entries[1]+entries[8]*mat.entries[2],
                entries[1]*mat.entries[0]+entries[5]*mat.entries[1]+entries[9]*mat.entries[2],
                entries[2]*mat.entries[0]+entries[6]*mat.entries[1]+entries[10]*mat.entries[2],
                0.0f,
                entries[0]*mat.entries[4]+entries[4]*mat.entries[5]+entries[8]*mat.entries[6],
                entries[1]*mat.entries[4]+entries[5]*mat.entries[5]+entries[9]*mat.entries[6],
                entries[2]*mat.entries[4]+entries[6]*mat.entries[5]+entries[10]*mat.entries[6],
                0.0f,
                entries[0]*mat.entries[8]+entries[4]*mat.entries[9]+entries[8]*mat.entries[10],
                entries[1]*mat.entries[8]+entries[5]*mat.entries[9]+entries[9]*mat.entries[10],
                entries[2]*mat.entries[8]+entries[6]*mat.entries[9]+entries[10]*mat.entries[10],
                0.0f,
                entries[0]*mat.entries[12]+entries[4]*mat.entries[13]+entries[8]*mat.entries[14]+entries[12],
                entries[1]*mat.entries[12]+entries[5]*mat.entries[13]+entries[9]*mat.entries[14]+entries[13],
                entries[2]*mat.entries[12]+entries[6]*mat.entries[13]+entries[10]*mat.entries[14]+entries[14],
                1.0f);
    }

    //Optimise for when bottom row of 1st matrix is (0, 0, 0, 1)
    if(    entries[3]==0.0f && entries[7]==0.0f && entries[11]==0.0f && entries[15]==1.0f)
    {
        return Matrix4x4(    entries[0]*mat.entries[0]+entries[4]*mat.entries[1]+entries[8]*mat.entries[2]+entries[12]*mat.entries[3],
                entries[1]*mat.entries[0]+entries[5]*mat.entries[1]+entries[9]*mat.entries[2]+entries[13]*mat.entries[3],
                entries[2]*mat.entries[0]+entries[6]*mat.entries[1]+entries[10]*mat.entries[2]+entries[14]*mat.entries[3],
                mat.entries[3],
                entries[0]*mat.entries[4]+entries[4]*mat.entries[5]+entries[8]*mat.entries[6]+entries[12]*mat.entries[7],
                entries[1]*mat.entries[4]+entries[5]*mat.entries[5]+entries[9]*mat.entries[6]+entries[13]*mat.entries[7],
                entries[2]*mat.entries[4]+entries[6]*mat.entries[5]+entries[10]*mat.entries[6]+entries[14]*mat.entries[7],
                mat.entries[7],
                entries[0]*mat.entries[8]+entries[4]*mat.entries[9]+entries[8]*mat.entries[10]+entries[12]*mat.entries[11],
                entries[1]*mat.entries[8]+entries[5]*mat.entries[9]+entries[9]*mat.entries[10]+entries[13]*mat.entries[11],
                entries[2]*mat.entries[8]+entries[6]*mat.entries[9]+entries[10]*mat.entries[10]+entries[14]*mat.entries[11],
                mat.entries[11],
                entries[0]*mat.entries[12]+entries[4]*mat.entries[13]+entries[8]*mat.entries[14]+entries[12]*mat.entries[15],
                entries[1]*mat.entries[12]+entries[5]*mat.entries[13]+entries[9]*mat.entries[14]+entries[13]*mat.entries[15],
                entries[2]*mat.entries[12]+entries[6]*mat.entries[13]+entries[10]*mat.entries[14]+entries[14]*mat.entries[15],
                mat.entries[15]);
    }

    //Optimise for when bottom row of 2nd matrix is (0, 0, 0, 1)
    if(    mat.entries[3]==0.0f && mat.entries[7]==0.0f &&
           mat.entries[11]==0.0f && mat.entries[15]==1.0f)
    {
        return Matrix4x4(    entries[0]*mat.entries[0]+entries[4]*mat.entries[1]+entries[8]*mat.entries[2],
                entries[1]*mat.entries[0]+entries[5]*mat.entries[1]+entries[9]*mat.entries[2],
                entries[2]*mat.entries[0]+entries[6]*mat.entries[1]+entries[10]*mat.entries[2],
                entries[3]*mat.entries[0]+entries[7]*mat.entries[1]+entries[11]*mat.entries[2],
                entries[0]*mat.entries[4]+entries[4]*mat.entries[5]+entries[8]*mat.entries[6],
                entries[1]*mat.entries[4]+entries[5]*mat.entries[5]+entries[9]*mat.entries[6],
                entries[2]*mat.entries[4]+entries[6]*mat.entries[5]+entries[10]*mat.entries[6],
                entries[3]*mat.entries[4]+entries[7]*mat.entries[5]+entries[11]*mat.entries[6],
                entries[0]*mat.entries[8]+entries[4]*mat.entries[9]+entries[8]*mat.entries[10],
                entries[1]*mat.entries[8]+entries[5]*mat.entries[9]+entries[9]*mat.entries[10],
                entries[2]*mat.entries[8]+entries[6]*mat.entries[9]+entries[10]*mat.entries[10],
                entries[3]*mat.entries[8]+entries[7]*mat.entries[9]+entries[11]*mat.entries[10],
                entries[0]*mat.entries[12]+entries[4]*mat.entries[13]+entries[8]*mat.entries[14]+entries[12],
                entries[1]*mat.entries[12]+entries[5]*mat.entries[13]+entries[9]*mat.entries[14]+entries[13],
                entries[2]*mat.entries[12]+entries[6]*mat.entries[13]+entries[10]*mat.entries[14]+entries[14],
                entries[3]*mat.entries[12]+entries[7]*mat.entries[13]+entries[11]*mat.entries[14]+entries[15]);
    }

    return Matrix4x4(    entries[0]*mat.entries[0]+entries[4]*mat.entries[1]+entries[8]*mat.entries[2]+entries[12]*mat.entries[3],
            entries[1]*mat.entries[0]+entries[5]*mat.entries[1]+entries[9]*mat.entries[2]+entries[13]*mat.entries[3],
            entries[2]*mat.entries[0]+entries[6]*mat.entries[1]+entries[10]*mat.entries[2]+entries[14]*mat.entries[3],
            entries[3]*mat.entries[0]+entries[7]*mat.entries[1]+entries[11]*mat.entries[2]+entries[15]*mat.entries[3],
            entries[0]*mat.entries[4]+entries[4]*mat.entries[5]+entries[8]*mat.entries[6]+entries[12]*mat.entries[7],
            entries[1]*mat.entries[4]+entries[5]*mat.entries[5]+entries[9]*mat.entries[6]+entries[13]*mat.entries[7],
            entries[2]*mat.entries[4]+entries[6]*mat.entries[5]+entries[10]*mat.entries[6]+entries[14]*mat.entries[7],
            entries[3]*mat.entries[4]+entries[7]*mat.entries[5]+entries[11]*mat.entries[6]+entries[15]*mat.entries[7],
            entries[0]*mat.entries[8]+entries[4]*mat.entries[9]+entries[8]*mat.entries[10]+entries[12]*mat.entries[11],
            entries[1]*mat.entries[8]+entries[5]*mat.entries[9]+entries[9]*mat.entries[10]+entries[13]*mat.entries[11],
            entries[2]*mat.entries[8]+entries[6]*mat.entries[9]+entries[10]*mat.entries[10]+entries[14]*mat.entries[11],
            entries[3]*mat.entries[8]+entries[7]*mat.entries[9]+entries[11]*mat.entries[10]+entries[15]*mat.entries[11],
            entries[0]*mat.entries[12]+entries[4]*mat.entries[13]+entries[8]*mat.entries[14]+entries[12]*mat.entries[15],
            entries[1]*mat.entries[12]+entries[5]*mat.entries[13]+entries[9]*mat.entries[14]+entries[13]*mat.entries[15],
            entries[2]*mat.entries[12]+entries[6]*mat.entries[13]+entries[10]*mat.entries[14]+entries[14]*mat.entries[15],
            entries[3]*mat.entries[12]+entries[7]*mat.entries[13]+entries[11]*mat.entries[14]+entries[15]*mat.entries[15]);

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator*(const float f) const
{
    return Matrix4x4(    entries[0]*f,
                        entries[1]*f,
                        entries[2]*f,
                        entries[3]*f,
                        entries[4]*f,
                        entries[5]*f,
                        entries[6]*f,
                        entries[7]*f,
                        entries[8]*f,
                        entries[9]*f,
                        entries[10]*f,
                        entries[11]*f,
                        entries[12]*f,
                        entries[13]*f,
                        entries[14]*f,
            entries[15]*f);
}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator/(const float f) const
{
    if (f==0.0f || f==1.0f)
        return (*this);

    float temp=1/f;

    return (*this)*temp;
}


bool AENGINE::Matrix4x4::operator==(const AENGINE::Matrix4x4 &mat) const
{

}

bool AENGINE::Matrix4x4::operator!=(const AENGINE::Matrix4x4 &mat) const
{

}

void AENGINE::Matrix4x4::operator+=(const AENGINE::Matrix4x4 &mat)
{

}

void AENGINE::Matrix4x4::operator-=(const AENGINE::Matrix4x4 &mat)
{

}

void AENGINE::Matrix4x4::operator*=(const AENGINE::Matrix4x4 &mat)
{

}

void AENGINE::Matrix4x4::operator*=(const float f)
{

}

void AENGINE::Matrix4x4::operator/=(const float f)
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator-() const
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::operator+() const {return (*this);}

AENGINE::Vector4 AENGINE::Matrix4x4::operator*(const AENGINE::Vector4 vec) const
{

}

void AENGINE::Matrix4x4::rotateVector3D(AENGINE::Vector3 &vec) const
{vec=getRotatedVector3D(vec);}

void AENGINE::Matrix4x4::inverseRotateVector3D(AENGINE::Vector3 &vec) const
{vec=getInverseRotatedVector3D(vec);}

AENGINE::Vector3 AENGINE::Matrix4x4::getRotatedVector3D(const AENGINE::Vector3 &vec) const
{

}

AENGINE::Vector3 AENGINE::Matrix4x4::getInverseRotatedVector3D(const AENGINE::Vector3 &vec) const
{

}

void AENGINE::Matrix4x4::translateVector3D(AENGINE::Vector3 &vec) const
{vec=getTranslatedVector3D(vec);}

void AENGINE::Matrix4x4::inverseTranslateVector3D(AENGINE::Vector3 &vec) const
{vec=getInverseTranslatedVector3D(vec);}

AENGINE::Vector3 AENGINE::Matrix4x4::getTranslatedVector3D(const AENGINE::Vector3 &vec) const
{

}

AENGINE::Vector3 AENGINE::Matrix4x4::getInverseTranslatedVector3D(const AENGINE::Vector3 &vec) const
{

}

void AENGINE::Matrix4x4::invert()
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::getInverse() const
{

}

void AENGINE::Matrix4x4::transpose()
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::getTranspose() const
{

}

void AENGINE::Matrix4x4::invertTranspose()
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::getInverseTranspose() const
{

}

void AENGINE::Matrix4x4::affineInvert()
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::getAffineInverse() const
{

}

void AENGINE::Matrix4x4::affineInvertTranspose()
{

}

AENGINE::Matrix4x4 AENGINE::Matrix4x4::getAffineInverseTranspose() const
{

}

void AENGINE::Matrix4x4::setTranslation(const AENGINE::Vector3 &translation)
{

}

void AENGINE::Matrix4x4::setScale(const AENGINE::Vector3 &scaleFactor)
{

}

void AENGINE::Matrix4x4::setUniformScale(const float scaleFactor)
{

}

void AENGINE::Matrix4x4::setRotationAxis(const double angle, const AENGINE::Vector3 &axis)
{

}

void AENGINE::Matrix4x4::setRotationX(const double angle)
{

}

void AENGINE::Matrix4x4::setRotationY(const double angle)
{

}

void AENGINE::Matrix4x4::setRotationZ(const double angle)
{

}

void AENGINE::Matrix4x4::setRotationEuler(const double angleX, const double angleY, const double angleZ)
{

}

void AENGINE::Matrix4x4::setPerspective(float left, float right, float bottom, float top, float n, float f)
{

}

void AENGINE::Matrix4x4::setPerspective(float fovy, float aspect, float n, float f)
{

}

void AENGINE::Matrix4x4::setOrtho(float left, float right, float bottom, float top, float n, float f)
{

}

void AENGINE::Matrix4x4::setTranslationPart(const AENGINE::Vector3 &translation)
{

}

void AENGINE::Matrix4x4::setRotationPartEuler(const AENGINE::Vector3 &rotations)
{
    setRotationPartEuler((double)rotations.getX(), (double)rotations.getY(), (double)rotations.getZ());
}

void AENGINE::Matrix4x4::setRotationPartEuler(const float angleX, const float angleY, const float angleZ)
{

}





