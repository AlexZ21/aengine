#include "adevice.h"
#include "math.h"

AENGINE::ADevice::ADevice()
{
    if (!glfwInit())
        return;

    sceneManager = new ASceneManager();

}

AENGINE::ADevice::~ADevice()
{
    delete sceneManager;
    delete window;
    Log::closeLog();
}

AENGINE::AWindow *AENGINE::ADevice::createWindow(int w, int h, bool fullscreen)
{
    window = new GLFWWindow(w, h, fullscreen);

    return window;
}

AENGINE::ASceneManager *AENGINE::ADevice::getSceneManager()
{
    return sceneManager;
}

void AENGINE::ADevice::initLog(const char *name, bool toConsole)
{
    Log::startLog(name, toConsole);
    Log::print()<<"Start programm";
}

void AENGINE::ADevice::update()
{
    if(window)
    {

        window->updateSizeWindow();
        glViewport(0, 0, window->getWidth(), window->getHeight());

        updatePerspective(45.0f, (GLfloat)window->getWidth() / (GLfloat)window->getHeight(), 0.0001f, 1000.0f);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        sceneManager->update();

        window->update();
    }
}

void AENGINE::ADevice::updatePerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar )
{
    GLfloat xmin, xmax, ymin, ymax;
    if(zNear == 0.0f) zNear = 1.0f;
    ymax = zNear * tan( fovy * M_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(xmin, xmax, ymin, ymax, zNear, zFar);
    glMatrixMode(GL_MODELVIEW);
}
