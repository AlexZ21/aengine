#ifndef AEVENT_H
#define AEVENT_H

namespace AENGINE {


enum AKey{
    AKEY_UNKNOWN = -1,
    AKEY_SPACE = 32,
    AKEY_APOSTROPHE = 39, /* ' */
    AKEY_COMMA = 44, /* , */
    AKEY_MINUS = 45, /* - */
    AKEY_PERIOD = 46, /* . */
    AKEY_SLASH = 47, /* / */

    AKEY_0 = 48,
    AKEY_1 = 49,
    AKEY_2 = 50,
    AKEY_3 = 51,
    AKEY_4 = 52,
    AKEY_5 = 53,
    AKEY_6 = 54,
    AKEY_7 = 55,
    AKEY_8 = 56,
    AKEY_9 = 57,

    AKEY_SEMICOLON = 59, /* ; */
    AKEY_EQUAL = 61, /* = */

    AKEY_A = 65,
    AKEY_B = 66,
    AKEY_C = 67,
    AKEY_D = 68,
    AKEY_E = 69,
    AKEY_F = 70,
    AKEY_G = 71,
    AKEY_H = 72,
    AKEY_I = 73,
    AKEY_J = 74,
    AKEY_K = 75,
    AKEY_L = 76,
    AKEY_M = 77,
    AKEY_N = 78,
    AKEY_O = 79,
    AKEY_P = 80,
    AKEY_Q = 81,
    AKEY_R = 82,
    AKEY_S = 83,
    AKEY_T = 84,
    AKEY_U = 85,
    AKEY_V = 86,
    AKEY_W = 87,
    AKEY_X = 88,
    AKEY_Y = 89,
    AKEY_Z = 90,

    AKEY_LEFT_BRACKET = 91, /* [ */
    AKEY_BACKSLASH = 92, /* \ */
    AKEY_RIGHT_BRACKET = 93, /* ] */
    AKEY_GRAVE_ACCENT = 96, /* ` */

    AKEY_WORLD_1 = 161, /* non-US #1 */
    AKEY_WORLD_2 = 162, /* non-US #2 */

    AKEY_ESCAPE = 256,
    AKEY_ENTER = 257,
    AKEY_TAB = 258,
    AKEY_BACKSPACE = 259,
    AKEY_INSERT = 260,
    AKEY_DELETE = 261,
    AKEY_RIGHT = 262,
    AKEY_LEFT = 263,
    AKEY_DOWN = 264,
    AKEY_UP = 265,

    AKEY_PAGE_UP = 265,
    AKEY_PAGE_DOWN = 267,
    AKEY_HOME = 268,
    AKEY_END = 269,

    AKEY_CAPS_LOCK = 280,
    AKEY_SCROLL_LOCK = 281,
    AKEY_NUM_LOCK = 282,
    AKEY_PRINT_SCREEN = 283,
    AKEY_PAUSE = 284,

    AKEY_F1 = 290,
    AKEY_F2 = 291,
    AKEY_F3 = 292,
    AKEY_F4 = 293,
    AKEY_F5 = 294,
    AKEY_F6 = 295,
    AKEY_F7 = 296,
    AKEY_F8 = 297,
    AKEY_F9 = 298,
    AKEY_F10 = 299,
    AKEY_F11 = 300,
    AKEY_F12 = 301,
    AKEY_F13 = 302,
    AKEY_F14 = 303,
    AKEY_F15 = 304,
    AKEY_F16 = 305,
    AKEY_F17 = 306,
    AKEY_F18 = 307,
    AKEY_F19 = 308,
    AKEY_F20 = 309,
    AKEY_F21 = 310,
    AKEY_F22 = 311,
    AKEY_F23 = 312,
    AKEY_F24 = 313,
    AKEY_F25 = 314,

    AKEY_KP_0 = 320,
    AKEY_KP_1 = 321,
    AKEY_KP_2 = 322,
    AKEY_KP_3 = 323,
    AKEY_KP_4 = 324,
    AKEY_KP_5 = 325,
    AKEY_KP_6 = 326,
    AKEY_KP_7 = 327,
    AKEY_KP_8 = 328,
    AKEY_KP_9 = 329,

    AKEY_KP_DECIMAL = 330,
    AKEY_KP_DIVIDE = 331,
    AKEY_KP_MULTIPLY = 332,
    AKEY_KP_SUBTRACT = 333,
    AKEY_KP_ADD = 334,
    AKEY_KP_ENTER = 335,
    AKEY_KP_EQUAL = 336,

    AKEY_LEFT_SHIFT = 340,
    AKEY_LEFT_CONTROL = 341,
    AKEY_LEFT_ALT = 342,
    AKEY_LEFT_SUPER = 343,
    AKEY_RIGHT_SHIFT = 344,
    AKEY_RIGHT_CONTROL = 345,
    AKEY_RIGHT_ALT = 346,
    AKEY_RIGHT_SUPER  = 347,
    AKEY_MENU = 348
};

enum AButton{
    ABUTTON_LEFT = 0,
    ABUTTON_CENTER = 2,
    ABUTTON_RIGHT = 1,
    ABUTTON_WHEEL_UP = 3,
    ABUTTON_WHEEL_DOWN = 4,
    ABUTTON_WHEEL_LEFT = 5,
    ABUTTON_WHEEL_RIGHT = 6,
    ABUTTON_8 = 7
};


class AEvent
{
public:
    AEvent(){}
    virtual ~AEvent(){}

    virtual bool keyPress(int key) = 0;
    virtual bool keyHit(int key) = 0;

    //Mouse
    virtual bool mousePress(int but) = 0;
    virtual bool mouseHit(int but) = 0;

    virtual void setCursorPostion(int x, int y) = 0;
    virtual double getCursorPosX() = 0;
    virtual double getCursorPosY() = 0;
    virtual double getCursorSpeedX() = 0;
    virtual double getCursorSpeedY() = 0;
};

}

#endif // AEVENT_H
