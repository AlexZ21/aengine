#ifndef ADEVICE_H
#define ADEVICE_H

#include "scene/ascenemanager.h"
#include "GLFW/glfwwindow.h"

namespace AENGINE {


class ADevice
{
    AWindow *window;
    ASceneManager *sceneManager;

    void updatePerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
public:
    ADevice();
    ~ADevice();
    AWindow *createWindow(int w, int h, bool fullscreen);
    ASceneManager *getSceneManager();

    void initLog(const char *name, bool toConsole);
    void update();
};

}

#endif // ADEVICE_H
