#ifndef AWINDOW_H
#define AWINDOW_H

#include "aevent.h"

namespace AENGINE {

class AWindow
{

public:
    AWindow(){}
    virtual ~AWindow(){}

    virtual void setWindowTitle(const char *title) = 0;
    virtual bool shouldExit() = 0;
    virtual void setShouldExit(bool e) = 0;

    virtual bool isFullscreen() = 0;
    virtual void setFullscreen(bool f) = 0;

    virtual void updateSizeWindow() = 0;
    virtual void update() = 0;

    virtual float getWidth() = 0;
    virtual float getHeight() = 0;

    virtual AEvent *getEventHandler() = 0;
};

}

#endif // AWINDOW_H
