TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lGL -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi

SOURCES += main.cpp \
    AEngine/adevice.cpp \
    AEngine/log.cpp \
    AEngine/GLFW/glfwwindow.cpp \
    AEngine/GLFW/glfwevent.cpp \
    AEngine/scene/ascene.cpp \
    AEngine/scene/ascenemanager.cpp \
    AEngine/scene/ascenenodegroup.cpp \
    AEngine/scene/scene_node/aabstractscenenode.cpp \
    AEngine/maths/vector3.cpp \
    AEngine/scene/scene_node/ascenesimplecubenode.cpp \
    AEngine/scene/scene_node/ascenesimplejetnode.cpp \
    AEngine/scene/scene_node/ascenecamera.cpp

HEADERS += \
    AEngine/aengine.h \
    AEngine/adevice.h \
    AEngine/awindow.h \
    AEngine/log.h \
    AEngine/aevent.h \
    AEngine/GLFW/glfwwindow.h \
    AEngine/GLFW/glfwevent.h \
    AEngine/scene/ascene.h \
    AEngine/scene/ascenemanager.h \
    AEngine/scene/ascenenodegroup.h \
    AEngine/scene/scene_node/aabstractscenenode.h \
    AEngine/maths/vector3.h \
    AEngine/scene/scene_node/ascenesimplecubenode.h \
    AEngine/scene/scene_node/ascenesimplejetnode.h \
    AEngine/scene/scene_node/ascenecamera.h

