#include "AEngine/aengine.h"

using namespace AENGINE;

int main(void)
{
    ADevice device;

    device.initLog("log.txt", true);

    AWindow *window = device.createWindow(800, 600, false);
    window->setWindowTitle("Тестовое окно");

    AEvent *event = window->getEventHandler();

    ASceneManager *sceneManager = device.getSceneManager();

    ASceneCamera *camera = sceneManager->createSceneCamera();

    AScene *scene = new AScene();
    sceneManager->addScene(scene);
    sceneManager->removeScene(scene);

    scene = new AScene(sceneManager);

    ASceneNodeGroup *nodeGroup = new ASceneNodeGroup();
    scene->addSceneNodeGroup(nodeGroup);

    ASceneSimpleCubeNode *cube = new ASceneSimpleCubeNode(scene->getRootSceneNodeGroup());
    cube->setPositionX(-2);

    ASceneSimpleJetNode *jet = new ASceneSimpleJetNode(scene->getRootSceneNodeGroup());
    jet->setPositionX(2);
    jet->setScale(vector3(0.02,0.02,0.02));

    float x = 20;
    float y = 0;
    float cubeRot = 0;

    double cameraRotX = 0, cameraRotY = 0;

    event->setCursorPostion(window->getWidth()/2, window->getHeight()/2);

    while(!window->shouldExit())
    {
        cameraRotX -= event->getCursorSpeedX()/3;
        cameraRotY -= event->getCursorSpeedY()/3;

        Log::print() << cameraRotX << " " << cameraRotY;

        if(event->getCursorPosY() > window->getHeight()/2)
        {
            event->setCursorPostion(window->getWidth()/2, window->getHeight()/2);
        } if(event->getCursorPosY() < window->getHeight()/2)
        {
            event->setCursorPostion(window->getWidth()/2, window->getHeight()/2);
        }

        if(event->getCursorPosX() > window->getWidth()/2)
        {
            event->setCursorPostion(window->getWidth()/2, window->getHeight()/2);
        } if(event->getCursorPosX() < window->getWidth()/2)
        {
            event->setCursorPostion(window->getWidth()/2, window->getHeight()/2);
        }


        if(event->keyPress(AKEY_SPACE) || event->keyHit(AKEY_ENTER))
            window->setShouldExit(true);

        if(event->keyHit(ABUTTON_LEFT) || event->mouseHit(ABUTTON_RIGHT))
            window->setShouldExit(true);

        if(event->keyHit(AKEY_A))
            if(scene->isVisible())
                scene->setVisible(false);
            else
                scene->setVisible(true);

        if(event->keyPress(AKEY_W))
            x += 1;
        if(event->keyPress(AKEY_S))
            x -= 1;
        camera->setPositionY(x);

        if(event->keyPress(AKEY_LEFT))
            y += 2;
        if(event->keyPress(AKEY_RIGHT))
            y -= 2;

        camera->setRotation(vector3(cameraRotY, cameraRotX, 0));

        cubeRot += 0.2;

        cube->setRotation(vector3(cubeRot,cubeRot,cubeRot));
        jet->setRotation(vector3(cubeRot,cubeRot,cubeRot));

        if(event->keyHit(AKEY_LEFT_SHIFT))
        {
            if(window->isFullscreen())
                window->setFullscreen(false);
            else
                window->setFullscreen(true);
        }

        device.update();
    }

}
